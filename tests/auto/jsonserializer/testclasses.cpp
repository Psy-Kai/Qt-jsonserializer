/****************************************************************************
**
** Copyright (C) 2017 Kai Dohmen <psykai1993 at googlemail dot com>.
**
** This file is part of Qt-jsonserializer.
**
** Qt-jsonserializer is free software: you can redistribute
** it and/or modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation, either version 3
** of the License, or (at your option) any later version.
**
** Qt-jsonserializer is distributed in the hope that it will be
** useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with Qt-jsonserializer.
** If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
**
****************************************************************************/

#include "testclasses.h"


TestSuper::TestSuper() :
    m_optr(Q_NULLPTR)
{}

const jsonserializer::Type TestSuper::serializableTypes(const int metaTypeId) const
{
    SERIALIZABLE_TYPE(metaTypeId, Test);
    SERIALIZABLE_TYPE(metaTypeId, Test*);
    SERIALIZABLE_TYPE(metaTypeId, TestBase*);
    SERIALIZABLE_TYPE(metaTypeId, QSharedPointer<Test>);
    SERIALIZABLE_TYPE(metaTypeId, QSharedPointer<TestBase>);
    SERIALIZABLE_TYPE(metaTypeId, QWeakPointer<Test>);

    return jsonserializer::Type();
}

Test TestSuper::object() const
{
    return m_object;
}

void TestSuper::setObject(const Test &object)
{
    m_object = object;
}

Test *TestSuper::optr() const
{
    return m_optr.data();
}

void TestSuper::setOptr(Test *optr)
{
    m_optr.reset(optr);
}

TestBase *TestSuper::optrbase() const
{
    return m_optrbase.data();
}

void TestSuper::setOptrbase(TestBase *optrbase)
{
    m_optrbase.reset(optrbase);
}

QSharedPointer<Test> TestSuper::sptr() const
{
    return m_sptr;
}

void TestSuper::setSptr(const QSharedPointer<Test> &sptr)
{
    m_sptr = sptr;
}

QSharedPointer<TestBase> TestSuper::sptrbase() const
{
    return m_sptrbase;
}

void TestSuper::setSptrbase(const QSharedPointer<TestBase> &sptr)
{
    m_sptrbase = sptr;
}

QWeakPointer<Test> TestSuper::wptr() const
{
    return m_wptr;
}

void TestSuper::setWptr(const QWeakPointer<Test> &wptr)
{
    m_wptr = wptr;
}

bool TestSuper::operator==(const TestSuper &other) const
{
    /* assume that every pointer valid */
    return (m_object == other.m_object)
            && (*m_optr == *other.m_optr)
            && (*m_optrbase == *other.m_optrbase)
            && (*m_sptr == *other.m_sptr)
            && (*m_sptrbase == *other.m_sptrbase)
            && (*m_wptr == *other.m_wptr);
}

TestQObject::TestQObject() :
    QObject()
{}

TestQObject::TestQObject(const TestQObject &other) :
    QObject()
{
    *this = other;
}

int TestQObject::value() const
{
    return m_value;
}

void TestQObject::setValue(int value)
{
    m_value = value;
}

TestQObject &TestQObject::operator=(const TestQObject &other)
{
    setName(other.name());
    m_value = other.m_value;
    return *this;
}

bool TestQObject::operator ==(const TestBase &other) const
{
    return Test::operator ==(other)
            && (m_value == static_cast<const TestQObject&>(other).m_value);
}

TestSuperQObject::TestSuperQObject() :
    QObject()
  , m_optr(Q_NULLPTR)
{}

const jsonserializer::Type TestSuperQObject::serializableTypes(const int metaTypeId) const
{
    SERIALIZABLE_TYPE(metaTypeId, TestQObject);
    SERIALIZABLE_TYPE(metaTypeId, TestQObject*);
    SERIALIZABLE_TYPE(metaTypeId, TestBase*);
    SERIALIZABLE_TYPE(metaTypeId, QSharedPointer<TestQObject>);
    SERIALIZABLE_TYPE(metaTypeId, QSharedPointer<TestBase>);
    SERIALIZABLE_TYPE(metaTypeId, QWeakPointer<TestQObject>);

    return jsonserializer::Type();
}

TestQObject TestSuperQObject::object() const
{
    return m_object;
}

void TestSuperQObject::setObject(const TestQObject &object)
{
    m_object = object;
}

TestQObject *TestSuperQObject::optr() const
{
    return m_optr.data();
}

void TestSuperQObject::setOptr(TestQObject *optr)
{
    m_optr.reset(optr);
}

TestBase *TestSuperQObject::optrbase() const
{
    return m_optrbase.data();
}

void TestSuperQObject::setOptrbase(TestBase *optrbase)
{
    m_optrbase.reset(optrbase);
}

QSharedPointer<TestQObject> TestSuperQObject::sptr() const
{
    return m_sptr;
}

void TestSuperQObject::setSptr(const QSharedPointer<TestQObject> &sptr)
{
    m_sptr = sptr;
}

QSharedPointer<TestBase> TestSuperQObject::sptrbase() const
{
    return m_sptrbase;
}

void TestSuperQObject::setSptrbase(const QSharedPointer<TestBase> &sptr)
{
    m_sptrbase = sptr;
}

QWeakPointer<TestQObject> TestSuperQObject::wptr() const
{
    return m_wptr;
}

void TestSuperQObject::setWptr(const QWeakPointer<TestQObject> &wptr)
{
    m_wptr = wptr;
}

bool TestSuperQObject::operator==(const TestSuperQObject &other) const
{
    /* assume that every pointer valid */
    return (m_object == other.m_object)
            && (*m_optr == *other.m_optr)
            && (*m_optrbase == *other.m_optrbase)
            && (*m_sptr == *other.m_sptr)
            && (*m_sptrbase == *other.m_sptrbase)
            && (*m_wptr == *other.m_wptr);
}

const jsonserializer::Type ListTest::serializableTypes(const int metaTypeId) const
{
    SERIALIZABLE_TYPE(metaTypeId, QList<Test>);
    SERIALIZABLE_TYPE(metaTypeId, QList<Test*>);
    SERIALIZABLE_TYPE(metaTypeId, QList<QSharedPointer<Test>>);

    return jsonserializer::Type();
}

QList<Test> ListTest::objectList() const
{
    return m_objectList;
}

void ListTest::setObjectList(const QList<Test> &objectList)
{
    m_objectList = objectList;
}

QList<Test *> ListTest::pointerList() const
{
    return m_pointerList;
}

void ListTest::setPointerList(const QList<Test *> &pointerList)
{
    m_pointerList = pointerList;
}

QList<QSharedPointer<Test> > ListTest::sharedPointerList() const
{
    return m_sharedPointerList;
}

void ListTest::setSharedPointerList(const QList<QSharedPointer<Test> > &sharedPointerList)
{
    m_sharedPointerList = sharedPointerList;
}

bool ListTest::operator==(const ListTest &other) const
{
    typedef QList<Test*> PointerList;
    typedef QList<QSharedPointer<Test>> SharedPointerList;

    auto pointerListTest = [](const PointerList &l, const PointerList &r) -> bool {
        if (l.size() != r.size())
            return false;
        auto i = l.size()-1;
        bool equal = true;
        while ((0 <= i) && equal) {
            equal = *l[i] == *r[i];
            i--;
        }
        return equal;
    };
    auto sharedPointerListTest = [](const SharedPointerList &l, const SharedPointerList &r) -> bool {
        if (l.size() != r.size())
            return false;
        auto i = l.size()-1;
        bool equal = true;
        while ((0 <= i) && equal) {
            equal = *l[i] == *r[i];
            i--;
        }
        return equal;
    };

    return (m_objectList == other.m_objectList)
            && pointerListTest(m_pointerList, other.m_pointerList)
            && sharedPointerListTest(m_sharedPointerList, other.m_sharedPointerList);
}

const jsonserializer::Type HashTest::serializableTypes(const int metaTypeId) const
{
    SERIALIZABLE_TYPE(metaTypeId, QHash<QString, Test>);

    return jsonserializer::Type();
}

QHash<QString, Test> HashTest::objectHash() const
{
    return m_objectHash;
}

void HashTest::setObjectHash(const QHash<QString, Test> &objectHash)
{
    m_objectHash = objectHash;
}

bool HashTest::operator==(const HashTest &other) const
{
    return m_objectHash == other.m_objectHash;
}

const jsonserializer::Type PairTest::serializableTypes(const int metaTypeId) const
{
    SERIALIZABLE_TYPE(metaTypeId, QPair<Test, QString>);

    return jsonserializer::Type();
}

QPair<Test, QString> PairTest::pair() const
{
    return m_pair;
}

void PairTest::setPair(const QPair<Test, QString> &pair)
{
    m_pair = pair;
}

bool PairTest::operator==(const PairTest &other) const
{
    return m_pair == other.m_pair;
}

const QMetaObject *EnumTest::metaObject() const
{
    return &EnumTest::staticMetaObject;
}

const jsonserializer::Type EnumTest::serializableTypes(const int metaTypeId) const
{
    return jsonserializer::Type();
    Q_UNUSED(metaTypeId)
}

EnumTest::SomeEnum EnumTest::someEnum() const
{
    return m_someEnum;
}

void EnumTest::setSomeEnum(const SomeEnum &someEnum)
{
    m_someEnum = someEnum;
}

bool EnumTest::operator==(const EnumTest &other) const
{
    return m_someEnum == other.m_someEnum;
}
