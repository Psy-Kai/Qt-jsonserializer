/****************************************************************************
**
** Copyright (C) 2017 Kai Dohmen <psykai1993 at googlemail dot com>.
**
** This file is part of Qt-jsonserializer.
**
** Qt-jsonserializer is free software: you can redistribute
** it and/or modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation, either version 3
** of the License, or (at your option) any later version.
**
** Qt-jsonserializer is distributed in the hope that it will be
** useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with Qt-jsonserializer.
** If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
**
****************************************************************************/

#pragma once

#include <QString>
#include "typehelper.h"

JSONSERIALIZER_BEGIN_NAMESPACE

struct ContainerItem
{
    static QJsonObject encapsulate(const QVariant &value, const Type &type);
    static QVariant decapsulate(const QJsonValue &value, const Type &type);
    static const QString kType;
    static const QString kData;
    static const QString kKey;
    static const QString kValue;
    static const qint32 kQSharedPointerLength;
};

struct Pointer
{
    static QJsonObject encapsulate(const QVariant &value, const Type &type);
    static QVariant decapsulate(const QJsonValue &value, const Type &type);
};

JSONSERIALIZER_END_NAMESPACE
