import qbs

Project {
    references: [
        "doc",
        "examples",
        "src",
        "tests",
    ]

    Product {
        name: "Licenses"

        files: [
            "COPYING",
            "COPYING.LESSER",
        ]
    }
}
