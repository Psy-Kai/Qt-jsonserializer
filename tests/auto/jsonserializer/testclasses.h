/****************************************************************************
**
** Copyright (C) 2017 Kai Dohmen <psykai1993 at googlemail dot com>.
**
** This file is part of Qt-jsonserializer.
**
** Qt-jsonserializer is free software: you can redistribute
** it and/or modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation, either version 3
** of the License, or (at your option) any later version.
**
** Qt-jsonserializer is distributed in the hope that it will be
** useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with Qt-jsonserializer.
** If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
**
****************************************************************************/

#pragma once

#include <QSharedPointer>
#include <jsonserializable.h>

class TestBase : public JsonSerializable
{
    Q_GADGET
public:
    virtual bool operator==(const TestBase &other) const { return true; Q_UNUSED(other) }
};
Q_DECLARE_METATYPE(TestBase*)

class Test : public TestBase
{
    Q_GADGET
    Q_PROPERTY(QString name READ name WRITE setName)
public:
    virtual const QMetaObject *metaObject() const Q_DECL_OVERRIDE { return &staticMetaObject; }
    virtual const jsonserializer::Type serializableTypes(const int metaTypeId) const Q_DECL_OVERRIDE { return jsonserializer::Type(); Q_UNUSED(metaTypeId) }
    QString name() const { return m_name; }
    void setName(const QString &name) { m_name = name; }
    virtual bool operator==(const TestBase &other) const { return m_name == static_cast<const Test&>(other).m_name; }
private:
    QString m_name;
};
Q_DECLARE_METATYPE(Test)
Q_DECLARE_METATYPE(Test*)

class TestDerived : public Test
{
    Q_GADGET
};
Q_DECLARE_METATYPE(TestDerived*)

class TestSuper : public TestBase
{
    Q_GADGET
    Q_PROPERTY(Test object READ object WRITE setObject)
    Q_PROPERTY(Test* optr READ optr WRITE setOptr)
    Q_PROPERTY(TestBase* optrbase READ optrbase WRITE setOptrbase)
    Q_PROPERTY(QSharedPointer<Test> sptr READ sptr WRITE setSptr)
    Q_PROPERTY(QSharedPointer<TestBase> sptrbase READ sptrbase WRITE setSptrbase)
    Q_PROPERTY(QWeakPointer<Test> wptr READ wptr WRITE setWptr)
public:
    explicit TestSuper();
    virtual const QMetaObject *metaObject() const Q_DECL_OVERRIDE { return &staticMetaObject; }
    virtual const jsonserializer::Type serializableTypes(const int metaTypeId) const Q_DECL_OVERRIDE;
    Test object() const;
    void setObject(const Test &object);
    Test *optr() const;
    void setOptr(Test *optr);
    TestBase *optrbase() const;
    void setOptrbase(TestBase *optrbase);
    QSharedPointer<Test> sptr() const;
    void setSptr(const QSharedPointer<Test> &sptr);
    QSharedPointer<TestBase> sptrbase() const;
    void setSptrbase(const QSharedPointer<TestBase> &sptr);
    QWeakPointer<Test> wptr() const;
    void setWptr(const QWeakPointer<Test> &wptr);
    bool operator==(const TestSuper &other) const;
private:
    Test m_object;
    QScopedPointer<Test> m_optr;
    QScopedPointer<TestBase> m_optrbase;
    QSharedPointer<Test> m_sptr;
    QSharedPointer<TestBase> m_sptrbase;
    QSharedPointer<Test> m_wptr;
};
Q_DECLARE_METATYPE(QSharedPointer<Test>)
Q_DECLARE_METATYPE(QSharedPointer<TestBase>)
Q_DECLARE_METATYPE(QWeakPointer<Test>)

class TestQObject : public QObject, public Test
{
    Q_OBJECT
    Q_PROPERTY(int value READ value WRITE setValue)
public:
    explicit TestQObject();
    TestQObject(const TestQObject &other);
    Q_INVOKABLE const QMetaObject *serializableMetaObject() const { return &Test::staticMetaObject; }
    int value() const;
    void setValue(int value);
    TestQObject &operator=(const TestQObject &other);
    virtual bool operator ==(const TestBase &other) const Q_DECL_OVERRIDE;
private:
    int m_value;
};
Q_DECLARE_METATYPE(TestQObject)
Q_DECLARE_METATYPE(TestQObject*)

class TestSuperQObject : public QObject, public TestBase
{
    Q_OBJECT
    Q_PROPERTY(TestQObject object READ object WRITE setObject)
    Q_PROPERTY(TestQObject* optr READ optr WRITE setOptr)
    Q_PROPERTY(TestBase* optrbase READ optrbase WRITE setOptrbase)
    Q_PROPERTY(QSharedPointer<TestQObject> sptr READ sptr WRITE setSptr)
    Q_PROPERTY(QSharedPointer<TestBase> sptrbase READ sptrbase WRITE setSptrbase)
    Q_PROPERTY(QWeakPointer<TestQObject> wptr READ wptr WRITE setWptr)
public:
    explicit TestSuperQObject();
    virtual const jsonserializer::Type serializableTypes(const int metaTypeId) const Q_DECL_OVERRIDE;
    TestQObject object() const;
    void setObject(const TestQObject &object);
    TestQObject *optr() const;
    void setOptr(TestQObject *optr);
    TestBase *optrbase() const;
    void setOptrbase(TestBase *optrbase);
    QSharedPointer<TestQObject> sptr() const;
    void setSptr(const QSharedPointer<TestQObject> &sptr);
    QSharedPointer<TestBase> sptrbase() const;
    void setSptrbase(const QSharedPointer<TestBase> &sptr);
    QWeakPointer<TestQObject> wptr() const;
    void setWptr(const QWeakPointer<TestQObject> &wptr);
    bool operator==(const TestSuperQObject &other) const;
private:
    TestQObject m_object;
    QScopedPointer<TestQObject> m_optr;
    QScopedPointer<TestBase> m_optrbase;
    QSharedPointer<TestQObject> m_sptr;
    QSharedPointer<TestBase> m_sptrbase;
    QSharedPointer<TestQObject> m_wptr;
};
Q_DECLARE_METATYPE(QSharedPointer<TestQObject>)
Q_DECLARE_METATYPE(QWeakPointer<TestQObject>)

class ListTest : public JsonSerializable
{
    Q_GADGET
    Q_PROPERTY(QList<Test> objectList READ objectList WRITE setObjectList)
    Q_PROPERTY(QList<Test*> pointerList READ pointerList WRITE setPointerList)
    Q_PROPERTY(QList<QSharedPointer<Test>> sharedPointerList READ sharedPointerList WRITE setSharedPointerList)
public:
    virtual const QMetaObject *metaObject() const Q_DECL_OVERRIDE { return &staticMetaObject; }
    virtual const jsonserializer::Type serializableTypes(const int metaTypeId) const Q_DECL_OVERRIDE;
    QList<Test> objectList() const;
    void setObjectList(const QList<Test> &objectList);
    QList<Test *> pointerList() const;
    void setPointerList(const QList<Test *> &pointerList);
    QList<QSharedPointer<Test> > sharedPointerList() const;
    void setSharedPointerList(const QList<QSharedPointer<Test> > &sharedPointerList);
    bool operator==(const ListTest &other) const;
private:
    QList<Test> m_objectList;
    QList<Test*> m_pointerList;
    QList<QSharedPointer<Test>> m_sharedPointerList;
};

class HashTest : public JsonSerializable
{
    Q_GADGET
    Q_PROPERTY(QHash<QString, Test> objectHash READ objectHash WRITE setObjectHash)
public:
    virtual const QMetaObject *metaObject() const Q_DECL_OVERRIDE { return &staticMetaObject; }
    virtual const jsonserializer::Type serializableTypes(const int metaTypeId) const Q_DECL_OVERRIDE;
    QHash<QString, Test> objectHash() const;
    void setObjectHash(const QHash<QString, Test> &objectHash);
    bool operator==(const HashTest &other) const;
private:
    QHash<QString, Test> m_objectHash;
};

class PairTest : public JsonSerializable
{
    Q_GADGET
    Q_PROPERTY(QPair<Test, QString> pair READ pair WRITE setPair)
public:
    virtual const QMetaObject *metaObject() const Q_DECL_OVERRIDE { return &staticMetaObject; }
    virtual const jsonserializer::Type serializableTypes(const int metaTypeId) const Q_DECL_OVERRIDE;
    QPair<Test, QString> pair() const;
    void setPair(const QPair<Test, QString> &pair);
    bool operator==(const PairTest &other) const;
private:
    QPair<Test, QString> m_pair;
};

class EnumTest : public JsonSerializable
{
    Q_GADGET
    Q_PROPERTY(SomeEnum someEnum READ someEnum WRITE setSomeEnum)
public:
    explicit EnumTest() : m_someEnum(First) {}
    enum SomeEnum { First, Second, Third };
    Q_ENUM(SomeEnum)
    virtual const QMetaObject *metaObject() const Q_DECL_OVERRIDE;
    virtual const jsonserializer::Type serializableTypes(const int metaTypeId) const Q_DECL_OVERRIDE;
    SomeEnum someEnum() const;
    void setSomeEnum(const SomeEnum &someEnum);
    bool operator==(const EnumTest &other) const;
private:
    SomeEnum m_someEnum;
};
