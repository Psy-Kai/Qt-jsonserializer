/****************************************************************************
**
** Copyright (C) 2017 Kai Dohmen <psykai1993 at googlemail dot com>.
**
** This file is part of Qt-jsonserializer.
**
** Qt-jsonserializer is free software: you can redistribute
** it and/or modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation, either version 3
** of the License, or (at your option) any later version.
**
** Qt-jsonserializer is distributed in the hope that it will be
** useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with Qt-jsonserializer.
** If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
**
****************************************************************************/

#include "serializetype.h"

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include "encapsulation.h"
#include "jsonserializable.h"
#include "jsonserializer.h"
#include "loggingcategory.h"

JSONSERIALIZER_BEGIN_NAMESPACE

Q_LOGGING_CATEGORY(serializeCategory, "jsonserializer.serialize")

SerializeType::SerializeType(const QVariant &value, const Type &type) :
    m_value(value)
  , m_type(type)
{}

QJsonValue SerializeType::process() const
{
    if (m_value.userType() < QMetaType::User) {
        qCDebug(serializeCategory) << "value of type" << QMetaType::typeName(m_value.userType());
        return QJsonValue::fromVariant(m_value);
    }

    if (!m_type.isValid()) {
        qCWarning(serializeCategory) << "unkown type";
        return QJsonValue();
    }

    const auto &helper = m_type.helper();
    if (helper->typeInfo(Type::HelperBase::IsSerializable)) {
        if (helper->typeInfo(Type::HelperBase::IsPointer)) {
            return Pointer::encapsulate(m_value, m_type);
        } else {
            JsonSerializer serializer;
            JsonSerializable *object;
            if (helper->typeInfo(Type::HelperBase::IsQObject)) {
                qCDebug(serializeCategory) << "value is QObject instance";
                object = dynamic_cast<JsonSerializable*>(jsonserializer::VariantCaster<QObject*>::castInstance(m_value));
            } else {
                qCDebug(serializeCategory) << "value is JsonSerializable instance";
                object = jsonserializer::VariantCaster<JsonSerializable*>::castInstance(m_value);
            }

            return serializer.serialize(*object);
        }
    }

    /* serialize container classes with special encapsulating objects:
         * { "type": "<typename>", "data": <data> }
         * <data> can be a standart type data or an object
         *
         * serialize lists this way:
         * [ { "type": "<typename>", "data": <data> } ]
         *
         * serialize hashs this way:
         * [ { "key": { "type": "<typename>", "data": <data> }, "value": { "type": "<typename>", "data": <data> } } ]
         *
         * serialize pair this way:
         * { "key": { "type": "<typename>", "data": <data> }, "value": { "type": "<typename>", "data": <data> } }
         */
    if (helper->typeInfo(Type::HelperBase::IsList)) {
        QJsonArray array;
        auto iterable = m_value.value<QSequentialIterable>();
        const auto itemType = helper->containerItemType().first();
        qCDebug(serializeCategory) << "value is list of" << QMetaType::typeName(itemType.helper()->metaTypeId());
        for (const auto &variant : iterable)
            array << ContainerItem::encapsulate(variant, itemType);
        return array;
    }

    if (helper->typeInfo(Type::HelperBase::IsHash)) {
        QJsonArray array;
        auto iterable = m_value.value<QAssociativeIterable>();
        const auto keyType = helper->containerItemType().first();
        const auto valueType = helper->containerItemType().last();
        qCDebug(serializeCategory) << "value is hash with key:" << QMetaType::typeName(keyType.helper()->metaTypeId()) << " and value:" << QMetaType::typeName(valueType.helper()->metaTypeId());
        for (auto &&it = iterable.begin(), &&itEnd = iterable.end(); it != itEnd; it++) {
            QJsonObject pair;
            pair[ContainerItem::kKey] = ContainerItem::encapsulate(it.key(), keyType);
            pair[ContainerItem::kValue] = ContainerItem::encapsulate(it.value(), valueType);
            array << pair;
        }
        return array;
    }

    if (helper->typeInfo(Type::HelperBase::IsPair)) {
        QJsonObject pair;
        const auto &&valuePair = m_value.value<QPair<QVariant, QVariant>>();
        const auto keyType = helper->containerItemType().first();
        const auto valueType = helper->containerItemType().last();
        qCDebug(serializeCategory) << "value is pair with first:" << QMetaType::typeName(keyType.helper()->metaTypeId()) << " and second:" << QMetaType::typeName(valueType.helper()->metaTypeId());
        pair[ContainerItem::kKey] = ContainerItem::encapsulate(valuePair.first, keyType);
        pair[ContainerItem::kValue] = ContainerItem::encapsulate(valuePair.second, valueType);
        return pair;
    }

    return QJsonValue();
}

JSONSERIALIZER_END_NAMESPACE
