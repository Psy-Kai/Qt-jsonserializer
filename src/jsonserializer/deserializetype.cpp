/****************************************************************************
**
** Copyright (C) 2017 Kai Dohmen <psykai1993 at googlemail dot com>.
**
** This file is part of Qt-jsonserializer.
**
** Qt-jsonserializer is free software: you can redistribute
** it and/or modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation, either version 3
** of the License, or (at your option) any later version.
**
** Qt-jsonserializer is distributed in the hope that it will be
** useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with Qt-jsonserializer.
** If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
**
****************************************************************************/

#include "deserializetype.h"

#include <QJsonArray>
#include <QJsonValue>
#include <QJsonObject>
#include "encapsulation.h"
#include "jsonserializable.h"
#include "jsonserializer.h"
#include "loggingcategory.h"

JSONSERIALIZER_BEGIN_NAMESPACE

Q_LOGGING_CATEGORY(deserializeCategory, "jsonserializer.deserialize")

DeserializeType::DeserializeType(const int valueMetaTypeId, const QJsonValue &value, const Type &type) :
    m_valueMetaTypeId(valueMetaTypeId)
  , m_value(value)
  , m_type(type)
{}

QVariant DeserializeType::process() const
{
    if (m_valueMetaTypeId < QMetaType::User) {
        qCDebug(deserializeCategory) << "value of type" << QMetaType::typeName(m_valueMetaTypeId);
        return m_value.toVariant();
    }

    if (!m_type.isValid()) {
        qCWarning(deserializeCategory) << "unkown type";
        return QVariant();
    }

    const auto &helper = m_type.helper();
    if (helper->typeInfo(Type::HelperBase::IsSerializable)) {
        if (helper->typeInfo(Type::HelperBase::IsPointer)) {
            return Pointer::decapsulate(m_value, m_type);
        } else {
            QScopedPointer<JsonSerializable> serializableObject;
            if (helper->typeInfo(Type::HelperBase::IsQObject)) {
                qCDebug(deserializeCategory) << "value is QObject instance";
                serializableObject.reset(dynamic_cast<JsonSerializable*>(reinterpret_cast<QObject*>(QMetaType::create(helper->metaTypeId()))));
            } else {
                qCDebug(deserializeCategory) << "value is JsonSerializable instance";
                serializableObject.reset(static_cast<JsonSerializable*>(QMetaType::create(helper->metaTypeId())));
            }
            JsonSerializer deserializer;
            deserializer.deserialize(serializableObject.data(), m_value.toObject());
            return helper->variantFrom(serializableObject.data());
        }
    }

    /* serialize container classes with special encapsulating objects:
         * { "type": "<typename>", "data": <data> }
         * <data> can be a standart type data or an object
         *
         * serialize lists this way:
         * [ { "type": "<typename>", "data": <data> } ]
         *
         * serialize hashs this way:
         * [ { "key": { "type": "<typename>", "data": <data> }, "value": { "type": "<typename>", "data": <data> } } ]
         *
         * serialize pair this way:
         * { "key": { "type": "<typename>", "data": <data> }, "value": { "type": "<typename>", "data": <data> } }
         */
    if (helper->typeInfo(Type::HelperBase::IsList)) {
        const auto itemType = helper->containerItemType().first();
        qCDebug(deserializeCategory) << "value is list of" << QMetaType::typeName(itemType.helper()->metaTypeId());
        for (const auto &item : m_value.toArray())
            helper->addToContainer({ ContainerItem::decapsulate(item, itemType) });
        return helper->variantFrom(Q_NULLPTR);
    }

    if (helper->typeInfo(Type::HelperBase::IsHash)) {
        const auto keyType = helper->containerItemType().first();
        const auto valueType = helper->containerItemType().last();
        qCDebug(deserializeCategory) << "value is hash with key:" << QMetaType::typeName(keyType.helper()->metaTypeId()) << " and value:" << QMetaType::typeName(valueType.helper()->metaTypeId());
        for (const auto &item : m_value.toArray()) {
            const auto &jsonObject = item.toObject();
            helper->addToContainer({ ContainerItem::decapsulate(jsonObject[ContainerItem::kKey], keyType), ContainerItem::decapsulate(jsonObject[ContainerItem::kValue], valueType) });
        }
        return helper->variantFrom(Q_NULLPTR);
    }

    if (helper->typeInfo(Type::HelperBase::IsPair)) {
        QJsonObject pair;
        const auto keyType = helper->containerItemType().first();
        const auto valueType = helper->containerItemType().last();
        qCDebug(deserializeCategory) << "value is pair with first:" << QMetaType::typeName(keyType.helper()->metaTypeId()) << " and second:" << QMetaType::typeName(valueType.helper()->metaTypeId());
        const auto &jsonObject = m_value.toObject();
        helper->addToContainer({ ContainerItem::decapsulate(jsonObject[ContainerItem::kKey], keyType), ContainerItem::decapsulate(jsonObject[ContainerItem::kValue], valueType) });
        return helper->variantFrom(Q_NULLPTR);
    }

    return QJsonValue();
}

JSONSERIALIZER_END_NAMESPACE
