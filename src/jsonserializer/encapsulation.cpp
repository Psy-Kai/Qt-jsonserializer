/****************************************************************************
**
** Copyright (C) 2017 Kai Dohmen <psykai1993 at googlemail dot com>.
**
** This file is part of Qt-jsonserializer.
**
** Qt-jsonserializer is free software: you can redistribute
** it and/or modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation, either version 3
** of the License, or (at your option) any later version.
**
** Qt-jsonserializer is distributed in the hope that it will be
** useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with Qt-jsonserializer.
** If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
**
****************************************************************************/

#include "encapsulation.h"

#include <QJsonObject>
#include "deserializetype.h"
#include "jsonserializable.h"
#include "jsonserializer.h"
#include "loggingcategory.h"
#include "serializetype.h"

JSONSERIALIZER_BEGIN_NAMESPACE

const QString ContainerItem::kType                  = QStringLiteral("type");
const QString ContainerItem::kData                  = QStringLiteral("data");
const QString ContainerItem::kKey                   = QStringLiteral("key");
const QString ContainerItem::kValue                 = QStringLiteral("value");
const qint32 ContainerItem::kQSharedPointerLength   = 15; // "QSharedPointer<"

QJsonObject ContainerItem::encapsulate(const QVariant &value, const Type &type)
{
    QJsonObject result;
    if (!type.isValid())
        return result;

    result[kType] = value.typeName();

    if (value.userType() < QMetaType::User) {
        result[kData] = QJsonValue::fromVariant(value);
    } else {
        SerializeType serialize(value, type);
        result[kData] = serialize.process();
    }

    return result;
}

QVariant jsonserializer::ContainerItem::decapsulate(const QJsonValue &value, const Type &type)
{
    const QJsonObject &jsonObject = value.toObject();

    auto metaTypeId = QMetaType::type(qPrintable(jsonObject[kType].toString()));
    if (metaTypeId == QMetaType::UnknownType)
        return QVariant();

    if (metaTypeId < QMetaType::User) {
        return jsonObject[kData].toVariant();
    } else {
        DeserializeType deserialize(metaTypeId, jsonObject[kData], type);
        return deserialize.process();
    }
}

QJsonObject Pointer::encapsulate(const QVariant &value, const Type &type)
{
    QJsonObject result;
    if (!type.isValid()) {
        qCWarning(serializeCategory) << "unkown type";
        return result;
    }

    if (value.userType() < QMetaType::User) {
        qCDebug(serializeCategory) << "value of type" << value.userType();
        result[ContainerItem::kType] = value.typeName();
        result[ContainerItem::kData] = QJsonValue::fromVariant(value);
    } else {
        const auto helper = type.helper();
        JsonSerializer serializer;
        const JsonSerializable *ptr = Q_NULLPTR;
        if (helper->typeInfo(Type::HelperBase::IsWeakPointer)) {
            if (helper->typeInfo(Type::HelperBase::IsQObject)) {
                qCDebug(serializeCategory) << "value is weakpointer to QObject";
                ptr = dynamic_cast<JsonSerializable*>(jsonserializer::VariantCaster<QWeakPointer<QObject>>::castInstance(value).toStrongRef().data());
            } else {
                qCDebug(serializeCategory) << "value is weakpointer to JsonSerializable";
                ptr = jsonserializer::VariantCaster<QWeakPointer<JsonSerializable>>::castInstance(value).toStrongRef().data();
            }
        } else {
            if (helper->typeInfo(Type::HelperBase::IsQObject)) {
                qCDebug(serializeCategory) << "value is" << (helper->typeInfo(Type::HelperBase::IsSharedPointer) ? "sharedpointer" : "pointer") << "to QObject";
                ptr = dynamic_cast<JsonSerializable*>(jsonserializer::VariantCaster<QObject*>::castPointer(value));
            } else {
                qCDebug(serializeCategory) << "value is" << (helper->typeInfo(Type::HelperBase::IsSharedPointer) ? "sharedpointer" : "pointer") << "to JsonSerializable";
                ptr = jsonserializer::VariantCaster<JsonSerializable*>::castPointer(value);
            }
        }

        if (ptr != Q_NULLPTR) {
            result[ContainerItem::kType] = ptr->metaObject()->className();
            result[ContainerItem::kData] = serializer.serialize(*ptr);
        }
    }

    return result;
}

QVariant Pointer::decapsulate(const QJsonValue &value, const Type &type)
{
    const QJsonObject &jsonObject = value.toObject();

    const QString typeName = jsonObject[ContainerItem::kType].toString();
    const auto metaTypeId = QMetaType::type(qPrintable(typeName));
    if (metaTypeId == QMetaType::UnknownType) {
        qCWarning(deserializeCategory) << "unkown type";
        return QVariant();
    }

    if (metaTypeId < QMetaType::User) {
        qCDebug(deserializeCategory) << "value of type" << typeName;
        return jsonObject[ContainerItem::kData].toVariant();
    } else {
        QScopedPointer<JsonSerializable> serializableObject;
        const auto metaTypeIdStar = QMetaType::type(qPrintable(typeName+QLatin1Char('*')));
        if (QMetaType::typeFlags(metaTypeIdStar) & QMetaType::PointerToQObject) {
            qCDebug(deserializeCategory) << "value is" << (type.helper()->typeInfo(Type::HelperBase::IsSharedPointer) ? "sharedpointer" : "pointer") << "to QObject";
            serializableObject.reset(dynamic_cast<JsonSerializable*>(reinterpret_cast<QObject*>(QMetaType::create(metaTypeId))));
        } else {
            qCDebug(deserializeCategory) << "value is" << (type.helper()->typeInfo(Type::HelperBase::IsSharedPointer) ? "sharedpointer" : "pointer") << "to JsonSerializable";
            serializableObject.reset(static_cast<JsonSerializable*>(QMetaType::create(metaTypeId)));
        }

        if (serializableObject == Q_NULLPTR) {
            qCWarning(deserializeCategory) << "Could not create new object of type" << typeName;
            return QVariant();
        }

        JsonSerializer deserializer;
        deserializer.deserialize(serializableObject.data(), jsonObject[ContainerItem::kData].toObject());
        return type.helper()->variantFrom(serializableObject.take());
    }
}

JSONSERIALIZER_END_NAMESPACE
