/****************************************************************************
**
** Copyright (C) 2017 Kai Dohmen <psykai1993 at googlemail dot com>.
**
** This file is part of Qt-jsonserializer.
**
** Qt-jsonserializer is free software: you can redistribute
** it and/or modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation, either version 3
** of the License, or (at your option) any later version.
**
** Qt-jsonserializer is distributed in the hope that it will be
** useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with Qt-jsonserializer.
** If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
**
****************************************************************************/

#include <QTest>

#include <QDebug>
#include <QJsonObject>
#include <jsonserializable.h>
#include <jsonserializer.h>

class DebuggingTest : public QObject
{
    Q_OBJECT
private:
    Q_SLOT void initTestCase();
    Q_SLOT void main();
};

void DebuggingTest::initTestCase()
{
}

class Base : public JsonSerializable
{
    Q_GADGET
public:
    virtual const QMetaObject *metaObject() const Q_DECL_OVERRIDE { return &staticMetaObject; }
    virtual const jsonserializer::Type serializableTypes(const int metaTypeId) const Q_DECL_OVERRIDE { return jsonserializer::Type(); Q_UNUSED(metaTypeId) }
    virtual void dummy() = 0;
};
Q_DECLARE_METATYPE(Base*)
Q_DECLARE_METATYPE(const Base*)
Q_DECLARE_METATYPE(QSharedPointer<Base>)

class A : public Base
{
    Q_GADGET
    Q_PROPERTY(QString value READ value WRITE setValue)
public:
    A() Q_DECL_EQ_DEFAULT;
    A(const A &other) : m_value(other.m_value) {}
    virtual const QMetaObject *metaObject() const { return &staticMetaObject; }
//    Q_INVOKABLE const QMetaObject *serializableMetaObject() const { return &Base::staticMetaObject; }
    virtual const jsonserializer::Type serializableTypes(const int metaTypeId) const Q_DECL_OVERRIDE { return jsonserializer::Type(); Q_UNUSED(metaTypeId) }
    virtual void dummy() Q_DECL_OVERRIDE {}
    QString value() const { return m_value; }
    void setValue(const QString &value) { m_value = value; }

private:
    QString m_value;
};
Q_DECLARE_METATYPE(A)

class B : public QObject, public JsonSerializable
{
    Q_OBJECT
    Q_PROPERTY(QSharedPointer<Base> ptr READ ptr WRITE setPtr)
    Q_PROPERTY(const Base* p READ p WRITE setP)
public:
    virtual const jsonserializer::Type serializableTypes(const int metaTypeId) const Q_DECL_OVERRIDE
    {
        SERIALIZABLE_TYPE(metaTypeId, QSharedPointer<Base>);
        SERIALIZABLE_CONST_TYPE(metaTypeId, Base*);

        return jsonserializer::Type();
    }
    QSharedPointer<Base> ptr() const { return m_ptr; }
    void setPtr(const QSharedPointer<Base> &ptr) { m_ptr = ptr; }
    const Base *p() const { return m_p.data(); }
    void setP(const Base *p) { m_p.reset(p); }
private:
    QSharedPointer<Base> m_ptr;
    QScopedPointer<const Base> m_p;
};

class Foo
{
public:
    void someFunction() {}
    void anotherFunction() {}
private:
    QString m_someString;
    qint64 m_someInt;
    QHash<QString, QString> m_someHash;
};

class Bar : public Foo, public JsonSerializable
{
    Q_GADGET
    Q_PROPERTY(QString someString READ someString WRITE setSomeString)
public:
    virtual const QMetaObject *metaObject() const Q_DECL_OVERRIDE Q_DECL_FINAL
    { return &staticMetaObject; }
    virtual const jsonserializer::Type serializableTypes(const int metaTypeId) const Q_DECL_OVERRIDE Q_DECL_FINAL
    { return jsonserializer::Type(); }
    QString someString() const;
    void setSomeString(const QString &someString);
private:
    QString m_someString;
};

void DebuggingTest::main()
{
    qDebug() << qMetaTypeId<A>();
//    qDebug() << qMetaTypeId<A*>();

    Bar bar;
    bar.setSomeString("YoloMcSwagger");
    const auto barJson = JsonSerializer::serialize(bar);
    auto *barPtr = JsonSerializer::deserialize<Bar>(barJson);
    qDebug() << barJson << barPtr->someString();

    B b;
    auto a = QSharedPointer<A>::create();
    a->setValue(QStringLiteral("Swag"));
    b.setPtr(a);
    auto *p = new A;
    p->setValue(QStringLiteral("Yolo"));
    b.setP(p);
    JsonSerializer serializer;
    const auto json = serializer.serialize(b);
    qDebug() << json;

//    B *bb = Q_NULLPTR;
    B *bb = serializer.deserialize<B>(json);
    A *aa = dynamic_cast<A*>(bb->ptr().data());
//    A *aa = dynamic_cast<A*>(bb->p());
    qDebug() << aa->value();
}



QString Bar::someString() const
{
return m_someString;
}

void Bar::setSomeString(const QString &someString)
{
m_someString = someString;
}

QTEST_APPLESS_MAIN(DebuggingTest)

#include "tst_debuggingtest.moc"
