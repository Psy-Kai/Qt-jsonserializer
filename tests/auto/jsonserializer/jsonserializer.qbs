import qbs

Product {
    files: [
        "testclasses.cpp",
        "testclasses.h",
        "tst_jsonserializer.cpp",
    ]
    name: "Autotest of Qt-jsonserializer"
    targetName: "tst_jsonserializer"
    type: [
        "application",
        "autotest",
    ]

    Depends {
        name: "cpp"
    }

    Depends {
        name: "Qt"
        submodules: [
            "core",
            "testlib",
        ]
    }

    Depends {
        name: "Jsonserializer"
    }
}
