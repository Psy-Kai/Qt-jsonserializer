/****************************************************************************
**
** Copyright (C) 2017 Kai Dohmen <psykai1993 at googlemail dot com>.
**
** This file is part of Qt-jsonserializer.
**
** Qt-jsonserializer is free software: you can redistribute
** it and/or modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation, either version 3
** of the License, or (at your option) any later version.
**
** Qt-jsonserializer is distributed in the hope that it will be
** useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with Qt-jsonserializer.
** If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
**
****************************************************************************/

#pragma once

#ifndef TYPEINFO_H
#define TYPEINFO_H

#include <QPair>
#include <QSharedPointer>
#include <QtGlobal>
#include <QVariant>

class JsonSerializable;
namespace jsonserializer {
//JSONSERIALIZER_BEGIN_NAMESPACE    

template <typename T>
struct VariantCaster {
    static T castInstance(const QVariant &variant)
    {
        return *static_cast<const T*>(variant.constData());
    }
    static T castPointer(const QVariant &variant)
    {
        return **static_cast<T**>(const_cast<void*>(variant.constData()));
    }
};

template <typename T>
struct VariantCaster<T*> {
    static T *castInstance(const QVariant &variant)
    {
        return static_cast<T*>(const_cast<void*>(variant.constData()));
    }
    static T *castPointer(const QVariant &variant)
    {
        return *static_cast<T**>(const_cast<void*>(variant.constData()));
    }
};

/* http://stackoverflow.com/questions/281725/template-specialization-based-on-inherit-class */
template<typename D, typename B>
class IsDerivedFrom
{
  class No { };
  class Yes { No no[3]; };

  static Yes Test( B* );
  static No Test( ... );

  static void constraints(D* p) { B* pb = p; pb = p; }

public:
  enum { Is = sizeof(Test(static_cast<D*>(0))) == sizeof(Yes) };

  IsDerivedFrom() { void(*p)(D*) = constraints; }
};

template <typename T, int>
struct IsImpl
{
    enum {
        value = false
    };
};

template <typename T>
struct IsImpl<T, 1>
{
    enum {
        value = true
    };
};

template <typename T>
struct IsSerializable : public IsImpl<T, IsDerivedFrom<T, JsonSerializable>::Is>
{};

template <typename T>
struct IsSerializable<T&> : public IsImpl<T, IsDerivedFrom<T, JsonSerializable>::Is>
{};

template <typename T>
struct IsQObject : public IsImpl<T, IsDerivedFrom<T, QObject>::Is>
{};

template <typename T>
struct IsQObject<T&> : public IsImpl<T, IsDerivedFrom<T, QObject>::Is>
{};

template <typename T>
struct IsList
{
    enum {
        value = false
    };
};

template <typename T>
struct IsList<QList<T>>
{
    enum {
        value = true
    };
};

template <typename T>
struct IsList<QVector<T>>
{
    enum {
        value = true
    };
};

template <typename T>
struct IsHash
{
    enum {
        value = false
    };
};

template <typename T1, typename T2>
struct IsHash<QHash<T1, T2>>
{
    enum {
        value = true
    };
};

template <typename T1, typename T2>
struct IsHash<QMap<T1, T2>>
{
    enum {
        value = true
    };
};

template <typename T>
struct IsPair
{
    enum {
        value = false
    };
};

template <typename T1, typename T2>
struct IsPair<QPair<T1, T2>>
{
    enum {
        value = true
    };
};

template <typename T>
struct TypeInfo
{
    enum {
        isSerializable = IsSerializable<T>::value,
        isQObject = IsQObject<T>::value,
        isList = IsList<T>::value,
        isHash = IsHash<T>::value,
        isPointer = QTypeInfo<T>::isPointer,
        isSharedPointer = false,
        isWeakPointer = false,
        isPair = IsPair<T>::value
    };
};

template <typename T>
struct TypeInfo<QSharedPointer<T>>
{
    enum {
        isSerializable = IsSerializable<T>::value,
        isQObject = IsQObject<T>::value,
        isList = IsList<T>::value,
        isHash = IsHash<T>::value,
        isPointer = true,
        isSharedPointer = true,
        isWeakPointer = false,
        isPair = IsPair<T>::value
    };
};

template <typename T>
struct TypeInfo<QWeakPointer<T>>
{
    enum {
        isSerializable = IsSerializable<T>::value,
        isQObject = IsQObject<T>::value,
        isList = IsList<T>::value,
        isHash = IsHash<T>::value,
        isPointer = true,
        isSharedPointer = false,
        isWeakPointer = true,
        isPair = IsPair<T>::value
    };
};

struct TypeInfoHelper
{
    template <typename T>
    static bool isSerializable(T type) { return TypeInfo<T>::isSerializable; Q_UNUSED(type) }
    template <typename T>
    static bool isSerializable(T *type) { return TypeInfo<T>::isSerializable; Q_UNUSED(type) }
    template <typename T>
    static bool isQObject(T type) { return TypeInfo<T>::isQObject; Q_UNUSED(type) }
    template <typename T>
    static bool isQObject(T *type) { return TypeInfo<T>::isQObject; Q_UNUSED(type) }
    template <typename T>
    static bool isList(T type) { return TypeInfo<T>::isList; Q_UNUSED(type) }
    template <typename T>
    static bool isHash(T type) { return TypeInfo<T>::isHash; Q_UNUSED(type) }
    template <typename T>
    static bool isPair(T type) { return TypeInfo<T>::isPair; Q_UNUSED(type) }
    template <typename T>
    static bool isPointer(T type) { return TypeInfo<T>::isPointer; Q_UNUSED(type) }
    template <typename T>
    static bool isSharedPointer(T type) { return TypeInfo<T>::isSharedPointer; Q_UNUSED(type) }
    template <typename T>
    static bool isWeakPointer(T type) { return TypeInfo<T>::isWeakPointer; Q_UNUSED(type) }
};

class Type
{
public:
    class HelperBase
    {
    public:
        enum TypeInfoValues {
            IsSerializable = 0,
            IsQObject,
            IsPointer,
            IsSharedPointer,
            IsWeakPointer,
            IsList,
            IsHash,
            IsPair,
            IsContainer
        };
        typedef TypeInfoValues TypeInfoValue;
        inline virtual ~HelperBase() {}
        virtual bool typeInfo(const TypeInfoValue value) const = 0;
        virtual int metaTypeId() const = 0;
        virtual QVariant variantFrom(void *value) const = 0;
        virtual QList<Type> containerItemType() const = 0;
        virtual void addToContainer(const QList<QVariant> &tuple) = 0;
    protected:
        template <typename T>
        static inline bool typeInfo(const TypeInfoValue value, T type)
        {
            switch (value) {
                case IsSerializable:
                    return TypeInfoHelper::isSerializable(type);
                    break;
                case IsQObject:
                    return TypeInfoHelper::isQObject(type);
                    break;
                case IsPointer:
                    return TypeInfoHelper::isPointer(type);
                    break;
                case IsSharedPointer:
                    return TypeInfoHelper::isSharedPointer(type);
                    break;
                case IsWeakPointer:
                    return TypeInfoHelper::isWeakPointer(type);
                    break;
                case IsContainer:
                    return typeInfo(IsList, type) || typeInfo(IsHash, type) || typeInfo(IsPair, type);
                    break;
                case IsList:
                    return TypeInfoHelper::isList(type);
                    break;
                case IsHash:
                    return TypeInfoHelper::isHash(type);
                    break;
                case IsPair:
                    return TypeInfoHelper::isPair(type);
                    break;
                default:
                    return false;
                    break;
            }

            return false;
        }
        template <typename T>
        static inline bool isRawPointer() { return (TypeInfo<T>::isPointer && !(TypeInfo<T>::isSharedPointer | TypeInfo<T>::isWeakPointer)); }
    };

    inline virtual ~Type() {}
    inline bool isValid() const { return m_helper != Q_NULLPTR; }
    inline int metaTypeId() const { return isValid() ? m_helper->metaTypeId() : 0; }
    inline const QSharedPointer<HelperBase> helper() const { return m_helper; }
    template <typename T>
    static Type create(){
        Type result;
        result.m_helper.reset(new Helper<T>());
        if (result.m_helper->typeInfo(HelperBase::IsContainer))
            result.m_helper.reset(new ContainerHelper<T>());

        return result;
    }
private:
    template <typename T>
    class Helper : public HelperBase
    {
    public:
        inline virtual ~Helper() Q_DECL_OVERRIDE {}
        inline virtual bool typeInfo(const TypeInfoValue value) const Q_DECL_OVERRIDE { return HelperBase::typeInfo(value, m_type); }
        inline virtual int metaTypeId() const Q_DECL_OVERRIDE { return qMetaTypeId<T>(); }
        inline virtual QVariant variantFrom(void *value) const Q_DECL_OVERRIDE { return QVariant::fromValue(TypeInfo<T>::isQObject ? *dynamic_cast<T*>(reinterpret_cast<JsonSerializable*>(value)) : *reinterpret_cast<T*>(value)); }
        inline virtual QList<Type> containerItemType() const Q_DECL_OVERRIDE { return {}; }
        inline virtual void addToContainer(const QList<QVariant> &tuple) { Q_UNUSED(tuple) }
    private:
        T m_type;
    };

    template <typename T>
    class Helper<T*> : public HelperBase
    {
    public:
        inline virtual ~Helper() Q_DECL_OVERRIDE {}
        inline virtual bool typeInfo(const TypeInfoValue value) const Q_DECL_OVERRIDE { return HelperBase::typeInfo(value, m_type); }
        inline virtual int metaTypeId() const Q_DECL_OVERRIDE { return qMetaTypeId<T*>(); }
        inline virtual QVariant variantFrom(void *value) const Q_DECL_OVERRIDE { return QVariant::fromValue(TypeInfo<T>::isQObject ? dynamic_cast<T*>(reinterpret_cast<JsonSerializable*>(value)) : reinterpret_cast<T*>(value)); }
        virtual QList<Type> containerItemType() const Q_DECL_OVERRIDE { return {}; }
        inline virtual void addToContainer(const QList<QVariant> &tuple) { Q_UNUSED(tuple) }
    private:
        T *m_type;
    };

    template <typename T>
    class Helper<QSharedPointer<T>> : public HelperBase
    {
    public:
        inline virtual ~Helper() Q_DECL_OVERRIDE {}
        inline virtual bool typeInfo(const TypeInfoValue value) const Q_DECL_OVERRIDE { return HelperBase::typeInfo(value, m_type); }
        inline virtual int metaTypeId() const Q_DECL_OVERRIDE { return qMetaTypeId<T*>(); }
        inline virtual QVariant variantFrom(void *value) const Q_DECL_OVERRIDE { return QVariant::fromValue(TypeInfo<T>::isQObject ? QSharedPointer<T>(dynamic_cast<T*>(reinterpret_cast<JsonSerializable*>(value))) : QSharedPointer<T>(reinterpret_cast<T*>(value))); }
        virtual QList<Type> containerItemType() const Q_DECL_OVERRIDE { return {}; }
        inline virtual void addToContainer(const QList<QVariant> &tuple) { Q_UNUSED(tuple) }
    private:
        QSharedPointer<T> m_type;
    };

    template <typename T>
    class Helper<QWeakPointer<T>> : public HelperBase
    {
    public:
        inline virtual ~Helper() Q_DECL_OVERRIDE {}
        inline virtual bool typeInfo(const TypeInfoValue value) const Q_DECL_OVERRIDE { return HelperBase::typeInfo(value, m_type); }
        inline virtual int metaTypeId() const Q_DECL_OVERRIDE { return qMetaTypeId<T*>(); }
        inline virtual QVariant variantFrom(void *value) const Q_DECL_OVERRIDE { TypeInfo<T>::isQObject ? m_value.reset(dynamic_cast<T*>(reinterpret_cast<JsonSerializable*>(value))) : m_value.reset(reinterpret_cast<T*>(value)); return QVariant::fromValue(m_value.toWeakRef()); }
        virtual QList<Type> containerItemType() const Q_DECL_OVERRIDE { return {}; }
        inline virtual void addToContainer(const QList<QVariant> &tuple) { Q_UNUSED(tuple) }
    private:
        QWeakPointer<T> m_type;
        mutable QSharedPointer<T> m_value;
    };

    template <typename T>
    class ContainerHelper : public Helper<T>
    {};

    template <typename T>
    class ContainerHelper<QList<T>> : public Helper<QList<T>>
    {
    public:
        inline virtual QVariant variantFrom(void *value) const Q_DECL_OVERRIDE { return QVariant::fromValue(m_list); Q_UNUSED(value) }
        inline virtual QList<Type> containerItemType() const
        {
            return { Type::create<T>() };
        }
        inline virtual void addToContainer(const QList<QVariant> &tuple) { m_list << (HelperBase::isRawPointer<T>() ? VariantCaster<T>::castPointer(tuple[0]) : VariantCaster<T>::castInstance(tuple[0])); }
    private:
        QList<T> m_list;
    };

    template <typename T>
    class ContainerHelper<QVector<T>> : public Helper<QVector<T>>
    {
    public:
        inline virtual QVariant variantFrom(void *value) const Q_DECL_OVERRIDE { return QVariant::fromValue(m_vector); Q_UNUSED(value) }
        inline virtual QList<Type> containerItemType() const
        {
            return { Type::create<T>() };
        }
        inline virtual void addToContainer(const QList<QVariant> &tuple) { m_vector << (HelperBase::isRawPointer<T>() ? VariantCaster<T>::castPointer(tuple[0]) : VariantCaster<T>::castInstance(tuple[0])); }
    private:
        QVector<T> m_vector;
    };

    template <typename T1, typename T2>
    class ContainerHelper<QHash<T1, T2>> : public Helper<QHash<T1, T2>>
    {
    public:
        inline virtual QVariant variantFrom(void *value) const Q_DECL_OVERRIDE { return QVariant::fromValue(m_hash); Q_UNUSED(value) }
        inline virtual QList<Type> containerItemType() const
        {
            return { Type::create<T1>(), Type::create<T2>() };
        }
        inline virtual void addToContainer(const QList<QVariant> &tuple) { m_hash.insert(HelperBase::isRawPointer<T1>() ? VariantCaster<T1>::castPointer(tuple[0]) : VariantCaster<T1>::castInstance(tuple[0])
              , HelperBase::isRawPointer<T2>() ? VariantCaster<T2>::castPointer(tuple[1]) : VariantCaster<T2>::castInstance(tuple[1])); }
    private:
        QHash<T1, T2> m_hash;
    };

    template <typename T1, typename T2>
    class ContainerHelper<QMap<T1, T2>> : public Helper<QHash<T1, T2>>
    {
    public:
        inline virtual QVariant variantFrom(void *value) const Q_DECL_OVERRIDE { return QVariant::fromValue(m_map); Q_UNUSED(value) }
        inline virtual QList<Type> containerItemType() const
        {
            return { Type::create<T1>(), Type::create<T2>() };
        }
        inline virtual void addToContainer(const QList<QVariant> &tuple) { m_map.insert(HelperBase::isRawPointer<T1>() ? VariantCaster<T1>::castPointer(tuple[0]) : VariantCaster<T1>::castInstance(tuple[1])
              , HelperBase::isRawPointer<T2>() ? VariantCaster<T2>::castPointer(tuple[1]) : VariantCaster<T2>::castInstance(tuple[1])); }
    private:
        QMap<T1, T2> m_map;
    };

    template <typename T1, typename T2>
    class ContainerHelper<QPair<T1, T2>> : public Helper<QPair<T1, T2>>
    {
    public:
        inline virtual QVariant variantFrom(void *value) const Q_DECL_OVERRIDE { return QVariant::fromValue(m_pair); Q_UNUSED(value) }
        inline virtual QList<Type> containerItemType() const
        {
            return { Type::create<T1>(), Type::create<T2>() };
        }
        inline virtual void addToContainer(const QList<QVariant> &tuple) { m_pair = qMakePair(HelperBase::isRawPointer<T1>() ? VariantCaster<T1>::castPointer(tuple[0]) : VariantCaster<T1>::castInstance(tuple[0])
                    , HelperBase::isRawPointer<T2>() ? VariantCaster<T2>::castPointer(tuple[1]) : VariantCaster<T2>::castInstance(tuple[1])); }
    private:
        QPair<T1, T2> m_pair;
    };

    QSharedPointer<HelperBase> m_helper;
};

}
//JSONSERIALIZER_END_NAMESPACE

#endif // TYPEINFO_H
