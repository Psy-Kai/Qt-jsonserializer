import qbs

Product {
    files: [
        "tst_debuggingtest.cpp",
    ]
    name: "Test for Debugging"
    targetName: "testForDebugging"
    type: [
        "application",
        "autotest",
    ]

    Depends {
        name: "cpp"
    }

    Depends {
        name: "Qt"
        submodules: [
            "core",
            "testlib",
        ]
    }

    Depends {
        name: "Jsonserializer"
    }
}
