import qbs

Project {
    name: "Auto Tests"

    references: [
        "debugging",
        "jsonserializer",
    ]

    AutotestRunner {
        Depends {
            name: "Test for Debugging"
        }
        Depends {
            name: "Autotest of Qt-jsonserializer"
        }
    }
}
