/****************************************************************************
**
** Copyright (C) 2017 Kai Dohmen <psykai1993 at googlemail dot com>.
**
** This file is part of Qt-jsonserializer.
**
** Qt-jsonserializer is free software: you can redistribute
** it and/or modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation, either version 3
** of the License, or (at your option) any later version.
**
** Qt-jsonserializer is distributed in the hope that it will be
** useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with Qt-jsonserializer.
** If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
**
****************************************************************************/

#include "jsonserializer.h"

#include <QJsonObject>
#include <QMetaProperty>
#include "deserializetype.h"
#include "jsonserializable.h"
#include "loggingcategory.h"
#include "serializetype.h"

const char *kSerializableMetaObject = "serializableMetaObject";

const QJsonObject JsonSerializer::serialize(const JsonSerializable &object)
{
    QJsonObject result;
    const auto *metaObject = object.metaObject();
    if (metaObject->inherits(&QObject::staticMetaObject)) {
        const auto *qobjectMetaObject = metaObject;
        const auto *qobject = dynamic_cast<const QObject*>(&object);
        if (!QMetaObject::invokeMethod(const_cast<QObject*>(qobject), kSerializableMetaObject, Q_RETURN_ARG(const QMetaObject*, metaObject)))
            metaObject = Q_NULLPTR;

        for (auto i = QObject::staticMetaObject.propertyCount(); i < qobjectMetaObject->propertyCount(); i++) {
            const auto &property = qobjectMetaObject->property(i);
            const auto &variant = property.read(qobject);
            if (property.isEnumType()) {
                const auto &metaEnum = property.enumerator();
                qCDebug(jsonserializer::serializeCategory) << "value of type" << metaEnum.name();
                result.insert(property.name(), metaEnum.valueToKey(variant.toInt()));
            } else {
                jsonserializer::SerializeType value(variant, object.serializableTypes(property.userType()));
                result.insert(property.name(), value.process());
            }
        }
    }

    if (metaObject != Q_NULLPTR) {
        for (auto i = JsonSerializable::staticMetaObject.propertyCount(); i < metaObject->propertyCount(); i++) {
            const auto &property = metaObject->property(i);
            const auto &variant = property.readOnGadget(&object);
            if (property.isEnumType()) {
                const auto &metaEnum = property.enumerator();
                qCDebug(jsonserializer::serializeCategory) << "value of type" << metaEnum.name();
                result.insert(property.name(), metaEnum.valueToKey(variant.toInt()));
            } else {
                jsonserializer::SerializeType value(variant, object.serializableTypes(property.userType()));
                result.insert(property.name(), value.process());
            }
        }
    }

    return result;
}

void JsonSerializer::deserializeHelper(JsonSerializable *object, const QJsonObject &jsonObject)
{
    const auto *metaObject = object->metaObject();
    if (metaObject->inherits(&QObject::staticMetaObject)) {
        const auto *qobjectMetaObject = metaObject;
        auto *qobject = dynamic_cast<QObject*>(object);
        if (!QMetaObject::invokeMethod(qobject, kSerializableMetaObject, Q_RETURN_ARG(const QMetaObject*, metaObject)))
            metaObject = Q_NULLPTR;

        for (auto i = QObject::staticMetaObject.propertyCount(); i < qobjectMetaObject->propertyCount(); i++) {
            const auto &property = qobjectMetaObject->property(i);
            if (property.isEnumType()) {
                const auto &metaEnum = property.enumerator();
                qCDebug(jsonserializer::deserializeCategory) << "value of type" << metaEnum.name();
                const auto &newValue = jsonObject.value(property.name()).toString();
                if (!property.writeOnGadget(object, newValue))
                    qCWarning(jsonserializer::deserializeCategory) << "Could not write value" << metaEnum.keyToValue(qPrintable(newValue)) << "to property" << property.name() << "of class" << metaObject->className();
            } else {
                const auto propertyType = property.userType();
                jsonserializer::DeserializeType value(propertyType, jsonObject.value(property.name()), object->serializableTypes(propertyType));
                const auto &newValue = value.process();
                if (!property.write(qobject, newValue))
                    qCWarning(jsonserializer::deserializeCategory) << "Could not write value" << newValue << "to property" << property.name() << "of class" << qobjectMetaObject->className();
            }
        }
    }

    if (metaObject != Q_NULLPTR) {
        for (auto i = JsonSerializable::staticMetaObject.propertyCount(); i < metaObject->propertyCount(); i++) {
            const auto &property = metaObject->property(i);
            if (property.isEnumType()) {
                const auto &metaEnum = property.enumerator();
                qCDebug(jsonserializer::deserializeCategory) << "value of type" << metaEnum.name();
                const auto &newValue = jsonObject.value(property.name()).toString();
                if (!property.writeOnGadget(object, newValue))
                    qCWarning(jsonserializer::deserializeCategory) << "Could not write value" << metaEnum.keyToValue(qPrintable(newValue)) << "to property" << property.name() << "of class" << metaObject->className();
            } else {
                const auto propertyType = property.userType();
                jsonserializer::DeserializeType value(propertyType, jsonObject.value(property.name()), object->serializableTypes(propertyType));
                const auto &newValue = value.process();
                if (!property.writeOnGadget(object, newValue))
                    qCWarning(jsonserializer::deserializeCategory) << "Could not write value" << newValue << "to property" << property.name() << "of class" << metaObject->className();
            }
        }
    }
}
