import qbs
import qbs.TextFile

Product {
    name: "Jsonserializer"
    targetName: "jsonserializer"
    type: [
        libraryType,
        "serializermodule"
    ]
    property string libraryType: "dynamiclibrary"

    cpp.defines: [
        "JSONSERIALIZER_LIBRARY",
        "JSONSERIALIZER_BEGIN_NAMESPACE=namespace jsonserializer {",
        "JSONSERIALIZER_END_NAMESPACE=}",
        "QT_NO_DEBUG_OUTPUT",
    ]
    cpp.includePaths: sourceDirectory
    cpp.cxxLanguageVersion: "c++11"

    files: [
        "deserializetype.cpp",
        "deserializetype.h",
        "encapsulation.cpp",
        "encapsulation.h",
        "jsonserializer.cpp",
        "loggingcategory.h",
        "serializetype.cpp",
        "serializetype.h",
    ]

    Group {
        name: "Public Headers"
        fileTags: [
            "hpp",
            "public_headers"
        ]
        files: [
            "jsonserializable.h",
            "jsonserializer.h",
            "typehelper.h",
            "jsonserializer_global.h",
        ]
    }

    Group {
        fileTagsFilter: libraryType
        qbs.install: true
        qbs.installDir: "lib"
    }

    Group {
        fileTagsFilter: "public_headers"
        qbs.install: true
        qbs.installDir: "include/"+product.targetName
    }

    Group {
        name: "Module Template"
        fileTags: [
            "serializermoduletemplate"
        ]
        files: "jsonserializerModule.qbs"
    }

    Rule {
        inputs: "serializermoduletemplate"
        Artifact {
            filePath: "serializer/"+input.fileName
            fileTags: "serializermodule"
        }
        prepare: {
            var cmd = new JavaScriptCommand();
            cmd.description = "Create Library Module";
            cmd.highlight = "codegen";
            cmd.sourceCode = function() {
                var file = new TextFile(input.filePath);
                var content = file.readAll();
                file.close()
                content = content.replace(/dummyIncludePath/g, '"'+input.moduleProperty("qbs", "installRoot")+'/include/'+product.targetName+'"');
                content = content.replace(/dummyLibraryPath/g, '"'+input.moduleProperty("qbs", "installRoot")+'/lib"');
                content = content.replace(/dummyLibrary/g, product.libraryType === "dynamiclibrary" ? "dynamicLibraries" : "staticLibraries");
                file = new TextFile(output.filePath, TextFile.WriteOnly);
                file.write(content);
                file.close();
            }
            return cmd;
        }
    }

    Depends {
        name: "cpp"
    }

    Depends {
        name: "Qt"
        submodules: [
            "core",
        ]
    }

    Export {
        Depends {
            name: "cpp"
        }

        Depends {
            name: "Qt"
            submodules: [
                "core",
            ]
        }

        cpp.includePaths: product.sourceDirectory
        cpp.cxxLanguageVersion: "c++11"
    }
}
