/****************************************************************************
**
** Copyright (C) 2017 Kai Dohmen <psykai1993 at googlemail dot com>.
**
** This file is part of Qt-jsonserializer.
**
** Qt-jsonserializer is free software: you can redistribute
** it and/or modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation, either version 3
** of the License, or (at your option) any later version.
**
** Qt-jsonserializer is distributed in the hope that it will be
** useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with Qt-jsonserializer.
** If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
**
****************************************************************************/

#pragma once

#ifndef JSONSERIALIZABLE_H
#define JSONSERIALIZABLE_H

#include <jsonserializer_global.h>
#include <typehelper.h>

class QMetaObject;
class JSONSERIALIZERSHARED_EXPORT JsonSerializable
{
    Q_GADGET
public:
    virtual ~JsonSerializable() {}
    virtual const QMetaObject *metaObject() const = 0;
    virtual const jsonserializer::Type serializableTypes(const int metaTypeId) const = 0;
};
#define SERIALIZABLE_TYPE(ID, TYPE...) if ((ID) == qMetaTypeId<TYPE>()) return jsonserializer::Type::create<TYPE>()
#define SERIALIZABLE_CONST_TYPE(ID, TYPE...) if ((ID) == qMetaTypeId<const TYPE>()) return jsonserializer::Type::create<TYPE>()

#endif // JSONSERIALIZABLE_H
