/****************************************************************************
**
** Copyright (C) 2017 Kai Dohmen <psykai1993 at googlemail dot com>.
**
** This file is part of Qt-jsonserializer.
**
** Qt-jsonserializer is free software: you can redistribute
** it and/or modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation, either version 3
** of the License, or (at your option) any later version.
**
** Qt-jsonserializer is distributed in the hope that it will be
** useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with Qt-jsonserializer.
** If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
**
****************************************************************************/

#include <QTest>

#include <typeinfo>
#include <QDebug>
#include <QJsonArray>
#include <QJsonObject>
#include <jsonserializer.h>
#include "testclasses.h"
#include <typehelper.h>

class JsonSerializerTest : public QObject
{
    Q_OBJECT
private:
    Q_SLOT void initTestCase();
    Q_SLOT void testVariantCasts();
    Q_SLOT void testBasicObjects();
    Q_SLOT void testComplexObject();
    Q_SLOT void testList();
    Q_SLOT void testHash();
    Q_SLOT void testPair();
    Q_SLOT void testQObjectDerived();
    Q_SLOT void testComplexQObjectDerived();
    Q_SLOT void testEnum();
};

void JsonSerializerTest::initTestCase()
{
    HashTest hash;
    hash.serializableTypes(0);
    PairTest pair;
    pair.serializableTypes(0);
}

void JsonSerializerTest::testVariantCasts()
{
    TestDerived foo;
    foo.setName(QStringLiteral("Some String"));

    QVariant variant = QVariant::fromValue(foo);
    QCOMPARE(jsonserializer::VariantCaster<Test>::castInstance(variant).name(), foo.name());
    QCOMPARE(jsonserializer::VariantCaster<Test*>::castInstance(variant)->name(), foo.name());

    variant = QVariant::fromValue(&foo);
    QCOMPARE(jsonserializer::VariantCaster<Test>::castPointer(variant).name(), foo.name());
    QCOMPARE(jsonserializer::VariantCaster<Test*>::castPointer(variant)->name(), foo.name());
}

void JsonSerializerTest::testBasicObjects()
{
    Test foo;
    QJsonObject barJson;
    foo.setName(QStringLiteral("Some String"));
    barJson["name"] = foo.name();
    const auto &json = JsonSerializer::serialize(foo);
    QCOMPARE(json, barJson);
    QCOMPARE(*QScopedPointer<Test>(JsonSerializer::deserialize<Test>(json)), foo);

    QBENCHMARK { JsonSerializer::serialize(foo); }
    Test benchmarkObj;
    QBENCHMARK { JsonSerializer::deserialize(&benchmarkObj, json); }
}

void JsonSerializerTest::testComplexObject()
{
    TestSuper foo;
    QJsonObject barJson;
    {
        auto &&o = foo.object();
        QJsonObject oJson;
        o.setName(QStringLiteral("Norbert"));
        oJson["name"] = o.name();
        foo.setObject(o);
        barJson["object"] = oJson;
        auto *p = new Test;
        QJsonObject pJson;
        p->setName(QStringLiteral("Martina"));
        pJson["name"] = p->name();
        foo.setOptr(p);
        barJson["optr"] = QJsonObject{ {"type", "Test"}, {"data", pJson} };
        auto *pBase = new Test;
        QJsonObject pBaseJson;
        pBase->setName(QStringLiteral("Tina"));
        pBaseJson["name"] = pBase->name();
        foo.setOptrbase(pBase);
        barJson["optrbase"] = QJsonObject{ {"type", "Test"}, {"data", pBaseJson} };
        const auto sptr = QSharedPointer<Test>::create();
        QJsonObject sptrJson;
        sptr->setName("Miriam");
        sptrJson["name"] = sptr->name();
        foo.setSptr(sptr);
        barJson["sptr"] = QJsonObject{ {"type", "Test"}, {"data", sptrJson} };
        auto *sptrbase = new Test;
        QJsonObject sptrbaseJson;
        sptrbase->setName("Miri");
        sptrbaseJson["name"] = sptrbase->name();
        foo.setSptrbase(QSharedPointer<TestBase>(sptrbase));
        barJson["sptrbase"] = QJsonObject{ {"type", "Test"}, {"data", sptrbaseJson} };
        const auto wptr = QSharedPointer<Test>::create();
        QJsonObject wptrJson;
        wptr->setName("Kai");
        wptrJson["name"] = wptr->name();
        foo.setWptr(wptr);
        barJson["wptr"] = QJsonObject{ {"type", "Test"}, {"data", wptrJson} };
    }
    const auto &json = JsonSerializer::serialize(foo);
    QCOMPARE(json, barJson);
    QCOMPARE(*QScopedPointer<TestSuper>(JsonSerializer::deserialize<TestSuper>(json)), foo);

    QBENCHMARK { JsonSerializer::serialize(foo); }
    TestSuper benchmarkObj;
    QBENCHMARK { JsonSerializer::deserialize(&benchmarkObj, json); }
}

void JsonSerializerTest::testList()
{
    ListTest foo;
    QJsonObject barJson;
    {
        auto &&list = foo.objectList();
        QJsonArray listJson;
        auto &&ptrList = foo.pointerList();
        QJsonArray ptrListJson;
        auto &&sPtrList = foo.sharedPointerList();
        QJsonArray sPtrListJson;
        for (int i = 3; 0 < i; i--) {
            Test item;
            QJsonObject itemJson;
            item.setName(QLatin1String("object")+QString::number(i));
            itemJson["name"] = item.name();
            list << item;
            listJson << QJsonObject{ {"type", "Test"}, {"data", itemJson} };
            auto *pItem = new Test;
            QJsonObject pItemJson;
            pItem->setName(QLatin1String("pointer")+QString::number(i));
            pItemJson["name"] = pItem->name();
            ptrList << pItem;
            ptrListJson << QJsonObject{ {"type", "Test*"}, {"data", QJsonObject{ {"type", "Test"}, {"data", pItemJson} }} };
            auto sItem = QSharedPointer<Test>::create();
            QJsonObject sItemJson;
            sItem->setName(QLatin1String("shared")+QString::number(i));
            sItemJson["name"] = sItem->name();
            sPtrList << sItem;
            sPtrListJson << QJsonObject{ {"type", "QSharedPointer<Test>"}, {"data", QJsonObject{ {"type", "Test"}, {"data", sItemJson} }} };
        }
        foo.setObjectList(list);
        barJson["objectList"] = listJson;
        foo.setPointerList(ptrList);
        barJson["pointerList"] = ptrListJson;
        foo.setSharedPointerList(sPtrList);
        barJson["sharedPointerList"] = sPtrListJson;
    }
    const auto &json = JsonSerializer::serialize(foo);
    QCOMPARE(json, barJson);
    QCOMPARE(*QScopedPointer<ListTest>(JsonSerializer::deserialize<ListTest>(json)), foo);

    QBENCHMARK { JsonSerializer::serialize(foo); }
    ListTest benchmarkObj;
    QBENCHMARK { JsonSerializer::deserialize(&benchmarkObj, json); }
}

void JsonSerializerTest::testHash()
{
    HashTest foo;
    QJsonObject barJson;
    {
        auto &&hash = foo.objectHash();
        QJsonArray hashJson;
        for (int i = 3; 0 < i; i--) {
            Test item;
            QJsonObject itemJson;
            item.setName(QLatin1String("object")+QString::number(i));
            itemJson["name"] = item.name();
            hash.insert(item.name(), item);
            hashJson << QJsonObject{ {"key", QJsonObject{ {"type", "QString"}, {"data", item.name()} }}, {"value", QJsonObject{ {"type", "Test"}, {"data", itemJson} }} };
        }
        foo.setObjectHash(hash);
        barJson["objectHash"] = hashJson;
    }
    const auto &json = JsonSerializer::serialize(foo);
    for (const auto &pair : json["objectHash"].toArray()) {
        bool found = true;
        int i = 0;
        const auto &hashJson = barJson["objectHash"].toArray();
        while (!found && (i < hashJson.size()))
            found = pair == barJson;

        if (!found)
            QFAIL("serialization of hash failed");
    }
    QCOMPARE(foo, *QScopedPointer<HashTest>(JsonSerializer::deserialize<HashTest>(json)));

    QBENCHMARK { JsonSerializer::serialize(foo); }
    HashTest benchmarkObj;
    QBENCHMARK { JsonSerializer::deserialize(&benchmarkObj, json); }
}

void JsonSerializerTest::testPair()
{
    PairTest foo;
    QJsonObject barJson;
    {
        auto &&pair = foo.pair();
        QJsonObject pairJson;
        pair.first.setName(QStringLiteral("Girl"));
        pairJson["key"] = QJsonObject{ {"type", "Test"}, {"data", QJsonObject{ {"name", pair.first.name()} }} };
        pair.second = QStringLiteral("Boy");
        pairJson["value"] = QJsonObject{ {"type", "QString"}, {"data", pair.second} };
        foo.setPair(pair);
        barJson["pair"] = pairJson;
    }
    const auto &json = JsonSerializer::serialize(foo);
    QCOMPARE(json, barJson);
    QCOMPARE(*QScopedPointer<PairTest>(JsonSerializer::deserialize<PairTest>(json)), foo);

    QBENCHMARK { JsonSerializer::serialize(foo); }
    PairTest benchmarkObj;
    QBENCHMARK { JsonSerializer::deserialize(&benchmarkObj, json); }
}

void JsonSerializerTest::testQObjectDerived()
{
    TestQObject foo;
    QJsonObject barJson;
    foo.setName(QStringLiteral("WIZO"));
    barJson["name"] = foo.name();
    foo.setValue(42);
    barJson["value"] = foo.value();
    const auto &json = JsonSerializer::serialize(foo);
    QCOMPARE(json, barJson);
    QCOMPARE(foo, *QScopedPointer<TestQObject>(JsonSerializer::deserialize<TestQObject>(json)));

    QBENCHMARK { JsonSerializer::serialize(foo); }
    TestQObject benchmarkObj;
    QBENCHMARK { JsonSerializer::deserialize(&benchmarkObj, json); }
}

void JsonSerializerTest::testComplexQObjectDerived()
{
    TestSuperQObject foo;
    QJsonObject barJson;
    {
        auto &&o = foo.object();
        QJsonObject oJson;
        o.setName(QStringLiteral("Norbert"));
        oJson["name"] = o.name();
        o.setValue(2);
        oJson["value"] = o.value();
        foo.setObject(o);
        barJson["object"] = oJson;
        auto *p = new TestQObject;
        QJsonObject pJson;
        p->setName(QStringLiteral("Martina"));
        pJson["name"] = p->name();
        p->setValue(20);
        pJson["value"] = p->value();
        foo.setOptr(p);
        barJson["optr"] = QJsonObject{ {"type", "TestQObject"}, {"data", pJson} };
        auto *pBase = new TestQObject;
        QJsonObject pBaseJson;
        pBase->setName(QStringLiteral("Tina"));
        pBaseJson["name"] = pBase->name();
        pBase->setValue(4);
        pBaseJson["value"] = pBase->value();
        foo.setOptrbase(pBase);
        barJson["optrbase"] = QJsonObject{ {"type", "TestQObject"}, {"data", pBaseJson} };
        const auto sptr = QSharedPointer<TestQObject>::create();
        QJsonObject sptrJson;
        sptr->setName("Miriam");
        sptrJson["name"] = sptr->name();
        sptr->setValue(18);
        sptrJson["value"] = sptr->value();
        foo.setSptr(sptr);
        barJson["sptr"] = QJsonObject{ {"type", "TestQObject"}, {"data", sptrJson} };
        auto *sptrbase = new TestQObject;
        QJsonObject sptrbaseJson;
        sptrbase->setName("Miri");
        sptrbaseJson["name"] = sptrbase->name();
        sptrbase->setValue(2);
        sptrbaseJson["value"] = sptrbase->value();
        foo.setSptrbase(QSharedPointer<TestBase>(sptrbase));
        barJson["sptrbase"] = QJsonObject{ {"type", "TestQObject"}, {"data", sptrbaseJson} };
        const auto wptr = QSharedPointer<TestQObject>::create();
        QJsonObject wptrJson;
        wptr->setName("Kai");
        wptrJson["name"] = wptr->name();
        wptr->setValue(26);
        wptrJson["value"] = wptr->value();
        foo.setWptr(wptr);
        barJson["wptr"] = QJsonObject{ {"type", "TestQObject"}, {"data", wptrJson} };
    }
    const auto &json = JsonSerializer::serialize(foo);
    QCOMPARE(json, barJson);
    QCOMPARE(*QScopedPointer<TestSuperQObject>(JsonSerializer::deserialize<TestSuperQObject>(json)), foo);

    QBENCHMARK { JsonSerializer::serialize(foo); }
    TestSuperQObject benchmarkObj;
    QBENCHMARK { JsonSerializer::deserialize(&benchmarkObj, json); }
}

void JsonSerializerTest::testEnum()
{
    EnumTest foo;
    QJsonObject barJson;
    foo.setSomeEnum(EnumTest::Third);
    barJson["someEnum"] = "Third";
    const auto &json = JsonSerializer::serialize(foo);
    QCOMPARE(json, barJson);
    QCOMPARE(*QScopedPointer<EnumTest>(JsonSerializer::deserialize<EnumTest>(json)), foo);

    QBENCHMARK { JsonSerializer::serialize(foo); }
    EnumTest benchmarkObj;
    QBENCHMARK { JsonSerializer::deserialize(&benchmarkObj, json); }
}

QTEST_APPLESS_MAIN(JsonSerializerTest)

#include "tst_jsonserializer.moc"
