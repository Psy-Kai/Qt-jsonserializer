/****************************************************************************
**
** Copyright (C) 2017 Kai Dohmen <psykai1993 at googlemail dot com>.
**
** This file is part of Qt-jsonserializer.
**
** Qt-jsonserializer is free software: you can redistribute
** it and/or modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation, either version 3
** of the License, or (at your option) any later version.
**
** Qt-jsonserializer is distributed in the hope that it will be
** useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with Qt-jsonserializer.
** If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
**
****************************************************************************/

#pragma once

#ifndef JSONSERIALIZER_GLOBAL_H
#define JSONSERIALIZER_GLOBAL_H

#include <QtGlobal>

#if defined(JSONSERIALIZER_LIBRARY)
#  define JSONSERIALIZERSHARED_EXPORT Q_DECL_EXPORT
#else
#  define JSONSERIALIZERSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // JSONSERIALIZER_GLOBAL_H
